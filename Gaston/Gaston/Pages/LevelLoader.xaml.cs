using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Gaston.Models;
using Gaston.Models.States;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Color = System.Drawing.Color;

namespace Gaston.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LevelLoader : ContentPage
    {
        private GamePage _currentPage;
        private readonly Level _level;
        private int _exampleCount;
        private const int LevelCap = 1000;
        private int _playerScore;
        private int _tracker;
        private Example _currentExample;

        public LevelLoader(Level level)
        {
            Example.Shuffle(level.Examples);
            _level = level;
            InitializeComponent();
            CreatePageInstance();
        }

        private void CreatePageInstance()
        {
            _currentExample = _level.Examples.ElementAt(_exampleCount);
            if (_currentExample.Score >= (LevelCap - _tracker))
            {
                _currentExample = _level.Examples.First(l => l.Score <= (LevelCap - _tracker));
            }

            _tracker += _currentExample.Score;

            if (_currentExample.GetType() == typeof(MultipleChoiceExample))
            {
                _currentPage = new MultipleChoicePage((MultipleChoiceExample) _currentExample);
            }
            else if (_currentExample.GetType() == typeof(FillBlankExample))
            {
                _currentPage = new FillBlankPage((FillBlankExample) _currentExample);
            }
            else if (_currentExample.GetType() == typeof(FingerPaintExample))
            {
                _currentPage = new FingerPaintPage((FingerPaintExample) _currentExample);
            }
            
            _currentPage.ExampleState.ExampleCompleted += OnLevelCompleted;
            Navigation.PushModalAsync(_currentPage);
            _level.Examples.Remove(_currentExample);
            _exampleCount++;
        }

        private void OnLevelCompleted(object source, EventArgs args)
        {
            
            if (_tracker < 1000)
                CreatePageInstance();
            else
            {
                Navigation.PushModalAsync(new NavigationPage(new ScoreScreen(_playerScore)));
                Navigation.PopModalAsync(true);
            }
        }
    }
}