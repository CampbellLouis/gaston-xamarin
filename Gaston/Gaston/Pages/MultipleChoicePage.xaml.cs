﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gaston.Models;
using Gaston.Models.States;
using Xamarin.Forms;

namespace Gaston.Pages
{
    public partial class MultipleChoicePage : GamePage
    {

        private readonly List<Button> _buttons = new List<Button>();
        private readonly MultipleChoiceExample _example;
        private readonly ScoreTracker _scoreTracker;
        public static int Score = 100;

        public MultipleChoicePage(MultipleChoiceExample example)
        {
            _scoreTracker = new ScoreTracker(6,100,0,1);
            ExampleState = new ExampleState();
            
            Random rand = new Random();
            example.MultipleChoiceVerb.Endings = example.MultipleChoiceVerb.Endings.OrderBy(x => rand.Next())
                .ToDictionary(item => item.Key, item => item.Value);
            
            InitializeComponent();
            
            _example = example;
            var test = ButtonGrid.GetChildElements(new Point());
            BindingContext = _example;
            
            for (int i = 0; i < _example.MultipleChoiceVerb.Endings.Count; i++)
            {
                var button = new Button()
                {
                    TextColor = Color.Black,
                    Text = _example.MultipleChoiceVerb.Endings.ElementAt(i).Key,
                    WidthRequest = 100,
                    BackgroundColor = Color.FromRgba(255, 198, 136, 255),
                    BorderWidth = 5,
                    BorderColor = Color.FromRgba(128, 128, 128, 255),
                    FontFamily = "CenturySchoolbook",
                    CornerRadius = 5
                    
                };
                Grid.SetRow(button, i);
                button.Clicked += OnButtonClicked;
                ButtonGrid.Children.Add(button);
                _buttons.Add(button);
            }
        }

        void DisableAllButtons()
        {
            foreach (var button in _buttons)
            {
                button.IsEnabled = false;
            }
        }
        void ExampleLost()
        {
            this.DisableAllButtons();
            this.BackgroundColor = Color.Red;
            Device.StartTimer(TimeSpan.FromSeconds(0.5), () =>
            {
                Navigation.PopModalAsync(false);
                this.ExampleState.Score = 0;
                this.ExampleState.Completed = true;
                return false;
            });
        }
        
        void ExampleWon()
        {
            this.DisableAllButtons();
            this.BackgroundColor = Color.Lime;
            Device.StartTimer(TimeSpan.FromSeconds(0.5), () =>
            {
                Navigation.PopModalAsync(false);
                this.ExampleState.Score = _scoreTracker.GetScore();
                this.ExampleState.Completed = true;                       
                return false;
            });
        }
        
        async void OnButtonClicked(object sender, EventArgs args)
        {
            Button button = (Button) sender;
            var test = _example.MultipleChoiceVerb.Endings.First(d => d.Value.Equals(true));
            if (_example.Sentence.Contains("_"))
            {
                _example.Sentence = _example.Sentence.Insert(_example.Sentence.IndexOf("_", StringComparison.Ordinal),
                    button.Text);
                _example.Sentence = _example.Sentence.Replace("_", "");
            }

            if (button.Text == test.Key)
            {
                this.ExampleWon();
            }
            else
            {
                this.ExampleLost();
            }
        }
    }
}