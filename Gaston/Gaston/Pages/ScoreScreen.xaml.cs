﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gaston.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gaston.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScoreScreen : GamePage
    {
        public ScoreScreen(int score)
        {
            InitializeComponent();
            double percentage = (score / 1000.0);
            scoreProgress.ProgressColor = Color.FromRgb(255, 15, 23);
            scoreProgress.ScaleY = 5;
            scoreProgress.Progress = percentage;
            
            
            
            Next.BackgroundColor = Color.FromRgb(255, 198, 136);
            Next.BorderWidth = 5;
            Next.BorderColor = Color.FromRgba(128, 128, 128, 75);
            Next.FontFamily = "CenturySchoolbook";
            Next.CornerRadius = 5;

            if (percentage >= 0.3) star1.Source = "bestStar.png";
            if (percentage >= 0.6) star2.Source = "bestStar.png";
            if (percentage >= 0.9) star3.Source = "bestStar.png";
        }

        private void Next_Clicked(object sender, EventArgs e)
        {
            var test = Navigation.ModalStack;
            Navigation.PopModalAsync(true);
            Navigation.PushAsync(new NavigationPage(new map1()));
        }
    }
}