﻿using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Collections.Generic;
using Gaston.TouchTracking;
using Xamarin.Forms;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using Gaston.Models;
using Gaston.Models.States;
using static SkiaSharp.SKBitmapResizeMethod;

namespace Gaston.Pages
{
    public class Box
    {
        public SKPoint Position;
        public readonly char C;

        public Box(int x, int y, Char c)
        {
            Position = new SKPoint(x, y);
            this.C = c;
        }
    }

    public partial class FingerPaintPage : GamePage
    {
        //current path being drawn onto canvas

        private readonly ScoreTracker _scoreTracker;

        
        private readonly SKPath _path = new SKPath();

        //list of all the boxes
        private readonly List<Box> _boxes = new List<Box>();

        //boxes already chosen 
        private readonly List<SKPoint> _crossedBoxes = new List<SKPoint>();

        //length of the side of one box, defined later
        private int _boxSize;

        //list with all the letters in the grid
        private List<char> _letters = new List<char>();

        //list of possible answers
        private List<string> _answers = new List<string>();

        //list of the sentences to complete
        private List<string> _sentences = new List<string>();

        //currently "created" word
        private string _selection = "";

        //Background
        SKBitmap _backgroundMap;
        
        

        private readonly SKPaint _paint = new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = SKColors.White,
            StrokeWidth = 10,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round
        };

        public FingerPaintPage(FingerPaintExample example)
        {
            InitializeComponent();
            ExampleState = new ExampleState();
            _scoreTracker = new ScoreTracker(90,ExampleState.Score,30,1.5f);

            List<char> l = example._letters;

            //initLetters(l);

            List<string> a = example._answers;

            //initAnswers(a);

            List<string> s = example._sentences;
            ;

            //initSentences(s);

            InitExample(l, a, s);

            //cuts long sentences so they don't go over the edge
            //CutStrings();


            // Load resource bitmap
            Assembly assembly = typeof(MainPage).GetTypeInfo().Assembly;


            using (Stream stream =
                assembly.GetManifestResourceStream($"{assembly.GetName().Name}.FingerPaintBackground.png"))
            {
                _backgroundMap = SKBitmap.Decode(stream);
            }
        }

        public void OnTouchEffectAction(object sender, TouchActionEventArgs args)
        {
            var test = ConvertToPixel(args.Location);

            if (!_answers.Contains(_selection))
                for (int col = 0; col < 4; col++)
                {
                    for (int row = 0; row < 4; row++)
                    {
                        int checkX = (int) _boxes[row + col * 4].Position.X;
                        int checkY = (int) _boxes[row + col * 4].Position.Y;
                        SKPoint center = new SKPoint(checkX + _boxSize / 2, checkY + _boxSize / 2);

                        if (_path.IsEmpty ||
                            test.X < _crossedBoxes[_crossedBoxes.Count - 1].X + 15.0 / 8 * _boxSize &&
                            test.X > _crossedBoxes[_crossedBoxes.Count - 1].X - 9.0 / 8 * _boxSize &&
                            test.Y < _crossedBoxes[_crossedBoxes.Count - 1].Y + 15.0 / 8 * _boxSize &&
                            test.Y > _crossedBoxes[_crossedBoxes.Count - 1].Y - 9.0 / 8 * _boxSize)
                        {
                            if (test.X < checkX + _boxSize * 7.0 / 8 &&
                                test.X > checkX + _boxSize * 1.0 / 8 &&
                                test.Y < checkY + _boxSize * 7.0 / 8 &&
                                test.Y > checkY + _boxSize * 1.0 / 8 &&
                                !_path.Contains(center.X, center.Y))
                            {
                                if (_path.IsEmpty)
                                {
                                    _path.Reset();
                                    _path.MoveTo(center);
                                    CanvasView.InvalidateSurface();
                                }

                                else
                                {
                                    _path.LineTo(center);

                                    for (int k = 0; k < 16; k++)
                                    {
                                        if (_boxes[k].Position + new SKPoint(_boxSize / 2, _boxSize / 2) == center)
                                        {
                                            _selection += _boxes[k].C;
                                            //Console.WriteLine(_boxes[k].c);

                                            if (_answers.Contains(_selection))
                                            {
                                                //DisplayAlert("Congratulations", "You have found an answer, press ok to ok", "ok");
                                                int count = 0;
                                                string check = _answers[count];
                                                while (check != _selection)
                                                {
                                                    count++;
                                                    check = _answers[count];
                                                }

                                                FillSentence(count);

                                                //reset selection after 1 sec
                                                Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                                                {
                                                    _crossedBoxes.Clear();
                                                    _path.Reset();
                                                    _selection = "";
                                                    CanvasView.InvalidateSurface();
                                                    return false;
                                                });
                                            }
                                        }

                                        if (_selection == "ree")
                                        {
                                            DisplayAlert("REEE", "REEEEEEEEEEEEEE", "REEEE");
                                        }
                                    }

                                    CanvasView.InvalidateSurface();
                                }

                                _crossedBoxes.Add(center);
                            }
                        }
                    }
                }

            switch (args.Type)
            {
                case TouchActionType.Pressed:
                    break;

                case TouchActionType.Moved:
                    _path.Reset();

                    if (_crossedBoxes.Count != 0 && !_answers.Contains(_selection))
                    {
                        _path.MoveTo(_crossedBoxes[0]);

                        foreach (SKPoint point in _crossedBoxes)
                        {
                            _path.LineTo(point);
                        }

                        _path.LineTo(ConvertToPixel(args.Location));
                        CanvasView.InvalidateSurface();
                    }

                    break;

                case TouchActionType.Released:
                    if (!_path.IsEmpty && !_answers.Contains(_selection))
                    {
                        _path.Reset();
                        _crossedBoxes.Clear();
                        _selection = "";
                        CanvasView.InvalidateSurface();
                    }

                    break;

                case TouchActionType.Cancelled:
                    if (!_path.IsEmpty)
                    {
                        CanvasView.InvalidateSurface();
                    }

                    break;
            }

            SKPoint ConvertToPixel(Point pt)
            {
                return new SKPoint((float) (CanvasView.CanvasSize.Width * pt.X / CanvasView.Width),
                    (float) (CanvasView.CanvasSize.Height * pt.Y / CanvasView.Height));
            }
        }


        public void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info;
            SKCanvas canvas = args.Surface.Canvas;
            int screenWidth = info.Width;
            int screenHeight = info.Height;
            long widthPercent = screenWidth / 100;
            long heightPercent = screenHeight / 100;
            _boxSize = (int) (18 * widthPercent);

            canvas.Clear();

            _backgroundMap =
                _backgroundMap.Resize(new SKImageInfo(screenWidth, screenHeight),
                    method: Lanczos3);
            canvas.DrawBitmap(_backgroundMap, 0, 0, _paint);

            int startX = (int) (20 * widthPercent);
            int startY = (int) (60 * heightPercent);

            for (int row = 0; row < 4; row++)
            {
                for (int col = 0; col < 4; col++)
                {
                    //init letters must be called before this
                    CreateBox(startX + col * _boxSize, startY + row * _boxSize, _letters[row + col * 4], canvas);
                }
            }

            //font 
            _paint.TextSize = 5 * heightPercent;
            _paint.IsAntialias = true;
            _paint.IsStroke = false;
            _paint.Typeface = SKTypeface.FromFamilyName(
                "Arial",
                SKFontStyleWeight.Bold,
                SKFontStyleWidth.Normal,
                SKFontStyleSlant.Upright);


            //draw letters
            for (int count = 0; count < 16; count++)
            {
                Box b = _boxes[count];
                String s = b.C.ToString();
                canvas.DrawText(s, (float) b.Position.X + _boxSize / 2 - 2 * widthPercent,
                    (float) b.Position.Y + _boxSize / 2 + 2 * heightPercent, _paint);
            }


            //draw sentences
            _paint.TextSize = 2 * heightPercent;
            canvas.DrawText(_selection, 10 * widthPercent, heightPercent * 10, _paint);
            int temp = (int) (8 * heightPercent);

            foreach (string s in _sentences)
            {
                canvas.DrawText(s, 5 * widthPercent, 10 * heightPercent + temp, _paint);
                temp += (int) (5 * heightPercent);
            }

            _paint.IsStroke = true;


            canvas.DrawPath(_path, _paint);

            void CreateBox(int x, int y, char cha, SKCanvas can)
            {
                canvas.DrawRect(x, y, _boxSize, _boxSize, _paint);
                _boxes.Add(new Box(x, y, cha));
            }
        }

        private void CutStrings()
        {
            int length = _sentences.Count;
            int temp = 0;

            List<string> newSentences = new List<string>();

            while (temp < length)
            {
                string s = _sentences[temp];

                if (s.Length > 20)
                {
                    char c = 'x';
                    int i = 15;
                    while (c != ' ')
                    {
                        c = s[i];
                        i++;
                    }

                    string sub1 = s.Substring(0, i - 1);
                    string sub2 = s.Substring(i);


                    newSentences.Add(sub1);
                    newSentences.Add(sub2);
                }
                else
                {
                    newSentences.Add(s);
                }


                temp++;
            }

            _sentences = newSentences;
        }

        private void FillSentence(int index)
        {
            string ans = _answers[index];
            int targetIndex = 0;
            int counter = 0;

            while (counter < index + 1)
            {
                if (_sentences[targetIndex].Contains("_"))
                {
                    counter++;
                }

                if (counter < index + 1)
                {
                    targetIndex++;
                }
            }

            int placement = _sentences[targetIndex].IndexOf("_", StringComparison.Ordinal);
            _sentences[targetIndex] = _sentences[targetIndex].Remove(placement, 1);
            _sentences[targetIndex] = _sentences[targetIndex].Insert(placement, _answers[index]);
            _answers.Remove(ans);
            if (_answers.Count == 0)
            {
                Navigation.PopModalAsync();
                ExampleState.Score = _scoreTracker.GetScore();
                ExampleState.Completed = true;
                
            }
                
        }

        private void InitLetters(List<char> letters)
        {
            _letters = letters;
        }

        private void InitAnswers(List<string> answers)
        {
            _answers = answers;
        }

        private void InitSentences(List<string> sentences)
        {
            _sentences = sentences;
        }

        private void InitExample(List<char> letters, List<string> answers, List<string> sentences)
        {
            InitLetters(letters);
            InitAnswers(answers);
            InitSentences(sentences);
        }
   }
}