using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gaston.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gaston.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class map1 : ContentPage
    {
        public map1()
        {
            InitializeComponent();
        }

        private void Level1_Clicked(object sender, EventArgs e)
        {
            var page = new StoryPage("1.gif");
            page.Disappearing += (sender2, e2) => { Navigation.PushAsync(new LevelLoader(Level.GetLevelFromJson())); };
            Navigation.PushModalAsync(page);
        }

        private void Level2_Clicked(object sender, EventArgs e)
        {
            var page = new StoryPage("2.gif");
            page.Disappearing += (sender2, e2) => { Navigation.PushAsync(new LevelLoader(Level.GetLevelFromJson())); };
            Navigation.PushModalAsync(page);
        }

        private void Level3_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new LevelLoader(Level.GetLevelFromJson()));
        }

        private void Level4_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new LevelLoader(Level.GetLevelFromJson()));
        }

        private void SwipeGestureRecognizer_Swiped(object sender, SwipedEventArgs e)
        {
            Navigation.PopModalAsync(true).Wait();
            Navigation.PushModalAsync(new map2()).Wait();
        }
    }
}