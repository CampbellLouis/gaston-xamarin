using System.Collections.Generic;

namespace Gaston.Models
{
    public class FingerPaintExample : Example
    {
        public List<string> _sentences;
        public List<char> _letters;
        public readonly List<string> _answers;
        private readonly ScoreTracker _scoreTracker;

        public FingerPaintExample(List<string> sentences, List<string> answers,List<char>letters)
        {
            Score = 500;
            _sentences = sentences;
            _answers = answers;
            _letters = letters;
            _scoreTracker = new ScoreTracker(90,500,100,1.5f);
        }
    }
}