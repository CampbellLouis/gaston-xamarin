using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Xml.Schema;
using Xamarin.Forms;

namespace Gaston.Models
{
    public class ScoreTracker
    {
        
        public delegate void TimeReachedZeroHandler(object source,EventArgs args);
        
        public event TimeReachedZeroHandler OutOfTime;
        
        private int _seconds;
        private readonly int _totalScore;
        private readonly int _minScore;
        private readonly float _penalty;
        private readonly Stopwatch _stopwatch = new Stopwatch();

        public ScoreTracker(int seconds, int totalScore, int minScore, float penalty)
        {
            _seconds = seconds;
            _totalScore = totalScore;
            _minScore = minScore;
            this._penalty = penalty;
            _stopwatch.Reset();
            Device.StartTimer(TimeSpan.FromSeconds(seconds), () =>
            {
                _stopwatch.Start();
                return false;
            });
            
        }

        public int GetElapsedSeconds()
        {
            if (_stopwatch.IsRunning)
            {
                _stopwatch.Stop();
                return (int) (_stopwatch.ElapsedMilliseconds / 1000);
            }

            return 0;

        }

        public int GetScore()
        {
            var test = GetElapsedSeconds();
            float score = _totalScore - (_penalty * GetElapsedSeconds());
            if (score > _minScore)
                return Convert.ToInt32(score);
            return _minScore;
        }
        
        protected virtual void OnOutOfTime(object source)
        {
            OutOfTime?.Invoke(source, EventArgs.Empty);
        }
    }
}